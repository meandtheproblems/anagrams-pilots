use std::ops::Add;

#[derive(Debug, Clone)]
pub struct FrequencyMap {
    pub map: [u8; 26]
}

#[derive(Debug, Clone)]
pub struct DictionaryWord {
    pub map: FrequencyMap,
    pub word: String
}

impl From<&String> for FrequencyMap {
    fn from(text: &String) -> Self {
        let mut m = FrequencyMap::new();
        for xc in text.chars().filter(|xc| xc.is_alphabetic()) {
            m.map[xc as usize - b'a' as usize] += 1;
        }
        m
    }
}

impl PartialEq for FrequencyMap {
    fn eq(&self, other: &FrequencyMap) -> bool {
        self.map == other.map
    }
}

impl Add for &FrequencyMap {
    type Output = FrequencyMap;

    fn add(self, other: &FrequencyMap) -> FrequencyMap {
        let mut m = FrequencyMap::new();
        let mut i: usize = 0;
        while i<26 {
            m.map[i] = self.map[i] + other.map[i];
            i += 1;
        }
        m
    }
}

impl FrequencyMap {
    pub fn new() -> Self {
        FrequencyMap {
            map: [0; 26]
        }
    }

    pub fn will_fit(&self, other: FrequencyMap) -> bool {
        // println!(">> {:?}", self.map);
        // println!("<< {:?}", other.map);
        let mut i: usize = 0;
        while i<26 {
            if self.map[i] < other.map[i] {
                // println!("Won't fit");
                return false;
            }
            i += 1;
        }
        // println!("Will fit");
        return true;
    }
}

impl From<&String> for DictionaryWord {
    fn from(word: &String) -> Self {
        DictionaryWord {
            map: FrequencyMap::from(word),
            word: word.clone()
        }
    }
}
