mod dictionary_word;

extern crate reqwest;
extern crate crypto;

use std::thread;
use std::error::Error;
use std::fs::File;
use std::io::prelude::*;
use std::io::ErrorKind;
use crypto::md5::Md5;
use crypto::digest::Digest;

use dictionary_word::{DictionaryWord, FrequencyMap};

#[derive(Debug, Clone)]
struct AnagramPhrase {
    phrase: String,
    words: String,
    frequency_map: FrequencyMap,
    source_frequency_map: FrequencyMap
}

#[derive(Clone)]
struct AnagramWordsDict {
    url: String,
    words: Vec<DictionaryWord>
}

impl AnagramPhrase {
    pub fn new(phrase: String) -> AnagramPhrase {
        let m = FrequencyMap::from(&phrase);
        AnagramPhrase {
            phrase,
            words: String::new(),
            frequency_map: FrequencyMap::new(),
            source_frequency_map: m
        }
    }

    pub fn clone_with_word(anagram: &AnagramPhrase, word: &DictionaryWord) -> Result<AnagramPhrase, Box<dyn Error>> {
        let mut new_anagram = anagram.clone();
        new_anagram.push(word)?;
        Ok(new_anagram)
    }

    pub fn generate_from_dict(thread_num: &usize, words_dict: &AnagramWordsDict, source_phrase: &AnagramPhrase, md5s: &Vec<String>) -> Vec<AnagramPhrase> {
        if source_phrase.is_complete() {
            if md5s.contains(&source_phrase.md5()) {
                println!("We have a match: [{}] - {}", source_phrase.words, source_phrase.md5());
                return vec![source_phrase.clone()];
            } else {
                //print!("-{}", thread_num);
                return Vec::new();
            }
        }
        let mut successful: Vec<AnagramPhrase> = Vec::new();
        for xw in words_dict.words.iter() {
            if let Ok(x) = AnagramPhrase::clone_with_word(source_phrase, xw) {
                // println!("V [{}] <<< {}", source_phrase.words, xw.word);
                successful.push(x);
            }
        }
        successful.iter().flat_map(|xs| AnagramPhrase::generate_from_dict(thread_num, words_dict, xs, md5s)).collect()
    }

    fn is_complete(&self) -> bool {
        self.frequency_map == self.source_frequency_map
    }

    fn will_fit(&self, word: &DictionaryWord) -> bool {
        return self.source_frequency_map.will_fit(&self.frequency_map + &word.map);
    }

    fn push(&mut self, word: &DictionaryWord) -> Result<(), Box<dyn Error>> {
        if !self.will_fit(word) {
            return Err(From::from(format!("{} + {} won't fit phrase {}", self.words, word.word, self.phrase)));
        }
        let new_word = word.clone();
        if self.words.len() > 0 {
            self.words.push_str(format!(" {}", new_word.word).as_str());
        } else {
            self.words.push_str(new_word.word.as_str());
        }
        self.frequency_map = FrequencyMap::from(&self.words);
        Ok(())
    }

    fn md5(&self) -> String {
        assert!(self.is_complete(), "Only calculate md5 when the anagram is complete!");
        let mut hasher = Md5::new();
        hasher.input(self.words.as_bytes());
        hasher.result_str()
    }

}

impl AnagramWordsDict {
    fn download_file(&self, mut f: &File) {
        println!("Loading from {}...", self.url);
        let text = reqwest::get(&self.url)
            .expect("Can't fetch")
            .text()
            .expect("Can't convert");
        println!("Read {} bytes from {}.", text.len(), self.url);
        f.write_all(text.as_bytes()).expect("Can't write dictionary");
    }

    fn load_dictionary(&mut self, phrase: &AnagramPhrase) {
        let mut text = String::new();
        println!("Try to open dict.txt...");
        let mut f = match File::open("dict.txt") {
            Ok(file) => {
                println!("File open...");
                file
            },
            Err(e) => match e.kind() {
                ErrorKind::NotFound => {
                    println!("Dictionary not found, let's try and create one...");
                    let w = File::create("dict.txt").expect("Unable to create file for dictionary");
                    self.download_file(&w);
                    File::open("dict.txt").expect("We have written the file, but we can't read it")
                },
                _ => panic!("Can't read file")
            }
        };
        println!("Reading from dict.txt");
        f.read_to_string(&mut text).expect("Can't read file");
        self.words = text.replace("\r\n", "\n").split('\n')
            .filter(|w| !w.is_empty())
            .filter(|w| w.is_ascii())
            .map(|w| DictionaryWord::from(&w.to_string()))
            .filter(|w| phrase.will_fit(w))
            .collect();
        self.words.dedup_by(|a, b| a.word == b.word);
        self.words.sort_unstable_by(|a, b| b.word.cmp(&a.word));
        println!("We have {} words in dictionary after cleaning", self.words.len());
    }
}

fn main() {
    const NTHREADS: usize = 72;
    let mut children = vec![];
    for thread_num in 0..NTHREADS {
        let hnd = thread::spawn(move || {
            let md5s: Vec<String> = vec![String::from("e4820b45d2277f3844eac66c903e84be"),
                String::from("23170acc097c24edb98fc5488ab033fe"),
                String::from("23170acc097c24edb98fc5488ab033fe")];
            let chunk_phrase = AnagramPhrase::new(String::from("poultry outwits ants"));
            let mut chunk_dict = AnagramWordsDict {
                url: String::from("https://followthewhiterabbit.trustpilot.com/cs/wordlist"),
                words: Vec::new()
            };            
            chunk_dict.load_dictionary(&chunk_phrase);

            let layer1: Vec<AnagramPhrase> = chunk_dict.words.iter()
                .map(|w| AnagramPhrase::clone_with_word(&chunk_phrase, w).unwrap())
                .collect();

            let chunk_len = layer1.len() / NTHREADS + 1;
            let chunks: Vec<Vec<AnagramPhrase>> = layer1.chunks(chunk_len).map(|xc| Vec::from(xc)).collect();
            let chunk = &chunks[thread_num];

            println!("Thread {} has got {} initial anagrams", thread_num, (*chunk).len());
            for xv in chunk {
                let chunk_variants = AnagramPhrase::generate_from_dict(&thread_num, &chunk_dict, &xv, &md5s);
                println!("Thread produced {} variants", chunk_variants.len());
            }
        });
        children.push(hnd);
    }
    for child in children {
        let _ = child.join();
    }
}
